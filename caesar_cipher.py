from art import logo

alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

print(logo)
def start():
    direction = input("\nType 'encode' to encrypt, type 'decode' to decrypt:\n").lower()
    text = input("Type your message:\n").lower()
    shift = int(input("Type the shift number:\n"))

    def caesar_cipher(input_text,shift_amount,direction):
        output_text = ""
        for letter in text:
            if letter in alphabet:
                alphabet_index = alphabet.index(letter)
                if direction == "encode":
                    alphabet_index_after_shift = alphabet_index + shift_amount
                elif direction == "decode":
                    alphabet_index_after_shift = alphabet_index - shift_amount
                if alphabet_index_after_shift < 0 or alphabet_index_after_shift > 25:
                    alphabet_index_after_shift = alphabet_index_after_shift % 26
                new_letter = alphabet[alphabet_index_after_shift]
            else:
                new_letter = letter
            output_text += new_letter

        print(f"The {direction}d text is {output_text}")

    caesar_cipher(input_text=text,shift_amount=shift,direction=direction )

    start_again = input("Want to restart the cipher program? Y/N: ").upper()
    if start_again == "Y":
        start()
    else:
        print("Goodbye!")

start()